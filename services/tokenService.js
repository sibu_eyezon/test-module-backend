const jwt = require('jsonwebtoken');
const { secretKey, tokenExpiry } = require('../config/config');

// generate a new token
const generateToken = (data) => jwt.sign({ data }, secretKey, { expiresIn: tokenExpiry });

// check the validity of the token, return Promise
// resolve the Promise if it is valid token
// otherwise reject the Promise
const verifyToken = (token) =>
  // eslint-disable-next-line implicit-arrow-linebreak
  new Promise((resolve, reject) => {
    // if token is blank then return false
    if (!token) {
      reject(new Error('token missing'));
    }
    // split the token with space
    // token should be of format `Bearer bnasmb23astuqw...`
    const splitted = token.split(' ');
    if (splitted.length !== 2) {
      reject(new Error('malformed token'));
    }
    const part = splitted[1];
    // verify token with tokenSecret
    // and return data if token is valid
    jwt.verify(part, secretKey, (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });

module.exports = { generateToken, verifyToken };
