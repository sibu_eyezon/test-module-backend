const pool = require('../db');
const {
  SELCT_RUNTST_CAN,
  SELCT_TST_PUB,
  SELCT_USR_DTLS,
  SELCT_EXAM_DTLS,
  UPDT_START_EXAM,
  UPDT_START_TIME,
  UPDT_END_TIME,
  SELCT_SECTION,
  SELCT_TOT_QUES,
  SELCT_QUES_SETS,
  SELCT_ATMPT_QA,
  SELCT_OPTS,
  SELCT_ATMPT_ANS,
  UPDT_ANSER,
  INS_RUN_TST,
  INS_RUN_TST_OPT,
  UPDT_END_TST_FLAG,
  UPDT_RUN_TST,
  SELCT_IN_PROGRESS_TST,
} = require('../queries/testQueries');

// verify passcode from DB
const passcodeVarification = (code) => pool.query(SELCT_RUNTST_CAN, [code]);

// select from test_pub by id
const getTestId = (id) => pool.query(SELCT_TST_PUB, [id]);

// get user details for user_details table by id
const getUserDetails = (id) => pool.query(SELCT_USR_DTLS, [id]);

// get exam details by id
const getExamDetails = (id) => pool.query(SELCT_EXAM_DTLS, [id]);

// update start_flag in DB to mark start of the exam
const startExam = (id) => pool.query(UPDT_START_EXAM, [id]);

// update the start time in DB
const setExamStartTime = (id, startTime) => pool.query(UPDT_START_TIME, [id, startTime]);

// update end time in DB
const setExamEndTime = (id, endTime) => pool.query(UPDT_END_TIME, [id, endTime]);

// get section names
const numberOfSection = (id) => pool.query(SELCT_SECTION, [id]);

// get total questions
const totalQuestionBySection = (id) => pool.query(SELCT_TOT_QUES, [id]);

// get question sets
const questionSets = (id, offset, maxLimit) => pool.query(SELCT_QUES_SETS, [id, offset, maxLimit]);

// get options by questions
const getOptionsByQuestion = (id) => pool.query(SELCT_OPTS, [id]);

// get quenstion and anser for a candidate
const getAttemptsQuestionAnswer = (id, section, testCan, pubID) =>
  pool.query(SELCT_ATMPT_QA, [id, section, testCan, pubID]);

// check attempt question answer for a candidate
const checkAttemptsQuestion = (id, section, testCan, pubID) =>
  pool.query(SELCT_ATMPT_ANS, [id, section, testCan, pubID]);

// update answer
const givenAnswerUpdate = (id, section, option, testCan, pubID) =>
  pool.query(UPDT_ANSER, [id, section, option, testCan, pubID]);

// insert into run_test table
const insertIntoRunTest = (pubID, secID, qID, testCan, qMarks) =>
  pool.query(INS_RUN_TST, [pubID, secID, qID, testCan, qMarks]);

// insert into run_test_option
const insertIntoRunTestOption = (runTest, option) => pool.query(INS_RUN_TST_OPT, [runTest, option]);

// update test end flag
const finishedTest = (id) => pool.query(UPDT_END_TST_FLAG, [id]);

// update end test flag
const endTest = (id) => pool.query(UPDT_RUN_TST, [id]);

// get in progress test details
const getOngoingTest = (testCan) => pool.query(SELCT_IN_PROGRESS_TST, [testCan]);

module.exports = {
  passcodeVarification,
  getTestId,
  getUserDetails,
  getExamDetails,
  startExam,
  setExamStartTime,
  setExamEndTime,
  numberOfSection,
  totalQuestionBySection,
  questionSets,
  getOptionsByQuestion,
  getAttemptsQuestionAnswer,
  checkAttemptsQuestion,
  givenAnswerUpdate,
  insertIntoRunTest,
  insertIntoRunTestOption,
  finishedTest,
  endTest,
  getOngoingTest,
};
