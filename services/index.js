const testService = require('./testService');
const tokenService = require('./tokenService');

module.exports = { testService, tokenService };
