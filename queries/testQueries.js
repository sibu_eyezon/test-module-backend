const SELCT_RUNTST_CAN = `SELECT * 
                            FROM runtest_can 
                            WHERE pswd = $1 
                            AND test_end_flag = false`;

const SELCT_TST_PUB = `SELECT * 
                        FROM test_pub
                        WHERE id = $1`;

const SELCT_USR_DTLS = `SELECT 
                            first_name, 
                            last_name, 
                            email, 
                            phone, 
                            photo_sm 
                        FROM user_details 
                        WHERE id = $1`;

const SELCT_EXAM_DTLS = `SELECT 
                            test.title as title, 
                            test.duration as time, 
                            test.marks as marks, 
                            program.title as program, 
                            program.code as code 
                        FROM test 
                        INNER JOIN program ON program.id = test.cat_id 
                        WHERE test.id = $1`;

const SELCT_SECTION = `SELECT 
                            id, 
                            section_name 
                        FROM section 
                        WHERE test_id = $1 
                        ORDER BY id`;

const SELCT_TOT_QUES = `SELECT 
                          ques.id, 
                          ques.type_id, 
                          ques.qbody 
                        FROM questions as ques 
                        INNER JOIN section_question_map as sqm ON sqm.ques_id = ques.id 
                        WHERE sqm.section_id = $1`;

const SELCT_QUES_SETS = `SELECT 
                            ques.id, 
                            ques.type_id, 
                            ques.qbody, 
                            ques.marks 
                          FROM questions as ques 
                          INNER JOIN section_question_map as sqm ON sqm.ques_id = ques.id 
                          WHERE sqm.section_id = $1 
                          ORDER BY ques.id OFFSET $2 LIMIT $3`;

const SELCT_OPTS = `SELECT 
                      id, 
                      body 
                    FROM options 
                    WHERE ques_id = $1`;

const SELCT_ATMPT_QA = `SELECT 
                          rto.option_id 
                        FROM run_test_option as rto 
                        INNER JOIN run_test as rt ON rto.run_test_id = rt.id 
                        WHERE rt.ques_id = $1 
                          AND rt.sec_id = $2 
                          AND rt.test_pub_id = $4 
                          AND rt.runtest_can_id = $3`;

const SELCT_ATMPT_ANS = `SELECT * 
                        FROM run_test 
                        WHERE ques_id = $1 
                          AND sec_id = $2 
                          AND test_pub_id = $4 
                          AND runtest_can_id = $3`;

const SELCT_IN_PROGRESS_TST = `SELECT * 
                                FROM runtest_can 
                                WHERE  id = $1 
                                AND test_start_flag = true 
                                AND test_end_flag = false`;

const UPDT_START_EXAM = `UPDATE runtest_can 
                        SET test_start_flag = true 
                        WHERE id = $1 RETURNING *`;

const UPDT_START_TIME = `UPDATE runtest_can 
                        SET test_start_dttm = $2 
                        WHERE id = $1 RETURNING *`;

const UPDT_END_TIME = `UPDATE runtest_can 
                        SET  test_end_dttm = $2 
                        WHERE id = $1 RETURNING *`;

const UPDT_ANSER = `UPDATE run_test_option 
                    SET option_id = $3 
                    WHERE run_test_id = (
                      SELECT 
                        id 
                      FROM run_test 
                      WHERE ques_id = $1 
                      AND sec_id = $2 
                      AND test_pub_id = $5 
                      AND runtest_can_id = $4
                    )`;

const INS_RUN_TST = `INSERT INTO run_test (
                      test_pub_id, 
                      sec_id, 
                      ques_id, 
                      runtest_can_id, 
                      marks
                    ) VALUES (
                      $1, 
                      $2, 
                      $3, 
                      $4, 
                      $5
                    ) RETURNING id`;

const INS_RUN_TST_OPT = `INSERT INTO run_test_option (
                          run_test_id, 
                          option_id, 
                          status, 
                          ans_status, 
                          marks
                        ) VALUES (
                          $1, 
                          $2, 
                          false, 
                          false, 
                          0
                        ) RETURNING *`;

const UPDT_END_TST_FLAG = `UPDATE runtest_can 
                            SET test_end_flag = true 
                          WHERE id = $1`;

const UPDT_RUN_TST = `UPDATE runtest_can 
                      SET test_end_flag = true 
                      WHERE id = $1`;

module.exports = {
  SELCT_RUNTST_CAN,
  SELCT_TST_PUB,
  SELCT_USR_DTLS,
  SELCT_EXAM_DTLS,
  SELCT_SECTION,
  SELCT_TOT_QUES,
  SELCT_QUES_SETS,
  SELCT_OPTS,
  SELCT_ATMPT_QA,
  SELCT_ATMPT_ANS,
  SELCT_IN_PROGRESS_TST,
  UPDT_START_EXAM,
  UPDT_START_TIME,
  UPDT_END_TIME,
  UPDT_ANSER,
  INS_RUN_TST,
  INS_RUN_TST_OPT,
  UPDT_END_TST_FLAG,
  UPDT_RUN_TST,
};
