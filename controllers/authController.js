const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { testService } = require('../services');
const { generateToken } = require('../services/tokenService');

// validate passcode and return the test details with a valid token
const passcodeAuthentication = async (req, res, next) => {
  const { passcode } = req.body;
  // validate required parameter passcode
  if (!passcode) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'passcode is required');
  }

  try {
    // query table with the passcode an get the data
    const passcodeData = await testService.passcodeVarification(passcode);

    // if data not found return error
    if (!passcodeData.rowCount) {
      throw new ApiError(httpStatus.FORBIDDEN, 'Invalid passcode provided');
    }
    // get test details associated with the passcode
    const testDetails = await testService.getTestId(passcodeData.rows[0].test_pub_id);
    // if test details not found then return error
    if (!testDetails.rowCount) {
      throw new ApiError(httpStatus.FORBIDDEN, 'test not found');
    }
    // construct data for the JWT token
    const data = { userID: passcodeData.rows[0].can_id, testID: testDetails.rows[0].test_id };
    const token = generateToken(data);
    // send success response
    res.json({
      token,
      test_can: passcodeData.rows[0].id,
      user_id: passcodeData.rows[0].can_id,
      test_id: testDetails.rows[0].test_id,
      pub_id: passcodeData.rows[0].test_pub_id,
    });
  } catch (err) {
    next(err);
  }
};

module.exports = { passcodeAuthentication };
