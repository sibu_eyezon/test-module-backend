const moment = require('moment');
const httpStatus = require('http-status');
const { testService } = require('../services');
const ApiError = require('../utils/ApiError');
const { TEST_TYPE } = require('../config/constants');

// when test is scheduled for any time
const anyTime = async (id, examDetails, entryTime, endTime) => {
  let test = examDetails;
  // if start time is not set previously then update the start time in DB
  if (!examDetails.rows[0]?.test_start_dttm) {
    test = await testService.setExamStartTime(id, entryTime);
  }
  // if the end time is not set previously then update the end time in DB
  if (!examDetails.rows[0].test_end_dttm) {
    test = await testService.setExamEndTime(id, endTime);
  }
  return test;
};

// when test is scheduled for a particular time
const particularTime = async (id, examDetails, testDetails, entryTime, duration) => {
  let test = examDetails;
  let testEndTime = null;

  if (!examDetails.rows[0]?.test_start_dttm) {
    const time = moment.utc(testDetails.rows[0].start_datetm);
    testEndTime = moment.utc(time).add(duration, 'minute');
    if (entryTime.isBefore(time)) {
      // when entryTime is before start time
      throw new ApiError(httpStatus.FORBIDDEN, 'Test not started Yet');
    } else if (entryTime.isAfter(testEndTime)) {
      // when entry time is after test end time
      throw new ApiError(httpStatus.FORBIDDEN, 'Test Aready finished');
    } else {
      test = await testService.setExamStartTime(id, time);
    }
  }

  // update test end time when not set already
  if (!test.rows[0].test_end_dttm) {
    test = await testService.setExamEndTime(id, testEndTime);
  }
  return test;
};

// when test is scheduled within a specific time range
const rangeTime = async (id, examDetails, testDetails, endTime, entryTime) => {
  let test;
  const pubStart = moment.utc(testDetails.rows[0].start_datetm);
  const pubEnd = moment.utc(testDetails.rows[0].end_datetm);

  if (!examDetails.rows[0]?.test_start_dttm) {
    if (entryTime.isBefore(pubStart)) {
      // when entry time is before start time
      throw new ApiError(httpStatus.FORBIDDEN, 'Test not started Yet');
    } else if (entryTime.isAfter(pubEnd)) {
      // when entry time is after end time
      throw new ApiError(httpStatus.FORBIDDEN, 'Test Aready finished');
    } else {
      test = await testService.setExamStartTime(id, entryTime);
    }
  }

  // update test end time
  if (moment.utc(endTime).isBefore(pubEnd)) {
    test = await testService.setExamEndTime(id, endTime);
  } else {
    test = await testService.setExamEndTime(id, pubEnd);
  }

  return test;
};

// query user_details table returns user details
const getUserDetails = async (req, res, next) => {
  try {
    let { id } = req.body;
    id = parseInt(id, 10);
    // check mandatory parameter id
    if (!id || Number.isNaN(id)) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'id value is required');
    }
    // get user details from the database
    const getUser = await testService.getUserDetails(id);
    // if user not found then return error
    if (!getUser.rowCount) {
      throw new ApiError(httpStatus.NOT_FOUND, 'user not found');
    }
    // return success response
    res.json({
      user_img: getUser.rows[0].photo_sm,
      user_info: {
        Name: `${getUser.rows[0].first_name} ${getUser.rows[0].last_name}`,
        Email: getUser.rows[0].email,
        Phone: getUser.rows[0].phone,
      },
    });
  } catch (error) {
    next(error);
  }
};

// query test table and return exam details
const getExamDetails = async (req, res, next) => {
  try {
    let { id } = req.body;
    id = parseInt(id, 10);
    // get exam details
    const getExam = await testService.getExamDetails(id);
    // if exam details not found then return error
    if (!getExam.rowCount) {
      throw new ApiError(httpStatus.NOT_FOUND, 'exam details not found');
    }
    // get number of sections
    const numOfSection = await testService.numberOfSection(id);
    // send success response
    res.json({
      Title: getExam.rows[0].title,
      Time: getExam.rows[0].time,
      Marks: getExam.rows[0].marks,
      Program: getExam.rows[0].program,
      Code: getExam.rows[0].code,
      Section: numOfSection.rowCount,
    });
  } catch (error) {
    next(error);
  }
};

// update the db table and mark the start of the exam
const startExam = async (req, res, next) => {
  try {
    let { id } = req.body;
    const { duration } = req.body;
    const currTime = moment.utc();
    const endTime = moment.utc(currTime).add(duration, 'minute');
    id = parseInt(id, 10);
    // check mandatory parameter id
    if ((!id, Number.isNaN(id))) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'id is required');
    }
    // update the table to mark start of exam
    const examDetails = await testService.startExam(id);
    // if exam details not found in DB
    if (examDetails.rowCount < 1) {
      throw new ApiError(httpStatus.NOT_FOUND, 'exam details not found');
    }
    // return error if end time is before current time
    if (examDetails.rows[0].test_start_dttm) {
      const testEndTime = moment.utc(examDetails.rows[0].test_start_dttm).add(duration, 'minute');
      if (moment.utc(testEndTime).isBefore(currTime)) {
        await testService.endTest(id);
        throw new ApiError(httpStatus.FORBIDDEN, 'Exam time finished already');
      }
    }
    const testPubID = examDetails.rows[0].test_pub_id;
    const testDetails = await testService.getTestId(testPubID);
    const testType = parseInt(testDetails?.rows[0]?.publish_type, 10);

    let result = null;
    if (testType === TEST_TYPE.ANY) {
      result = await anyTime(id, examDetails, currTime, endTime);
    } else if (testType === TEST_TYPE.PARTICULAR) {
      result = await particularTime(id, examDetails, testDetails, currTime, duration);
    } else if (testType === TEST_TYPE.RANGE) {
      result = await rangeTime(id, examDetails, testDetails, endTime, currTime);
    }
    if (!result) {
      throw new Error('Unexpected error occurred');
    }
    res.send(result.rows[0]);
  } catch (err) {
    next(err);
  }
};

module.exports = { getUserDetails, getExamDetails, startExam };
