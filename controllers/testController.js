const httpStatus = require('http-status');
const { testService } = require('../services');
const ApiError = require('../utils/ApiError');

// get details of sections
const getTotalSection = async (req, res, next) => {
  try {
    let { id } = req.body;
    id = parseInt(id, 10);
    // check mandatory parameter id
    if (!id || Number.isNaN(id)) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'id is required');
    }

    // get the details from the DB
    const getSection = await testService.numberOfSection(id);
    res.json(getSection.rows);
  } catch (error) {
    next(error);
  }
};

// get details of question sets
const questionSets = async (req, res, next) => {
  try {
    let { id, page, maxLimit } = req.body;
    id = parseInt(id, 10);
    page = parseInt(page, 10);
    maxLimit = parseInt(maxLimit, 10) || 1;
    const offset = (page - 1) * maxLimit;

    // get total question by section
    const totalQuesBySec = await testService.totalQuestionBySection(id);
    // if details are not found then return error
    if (!totalQuesBySec.rowCount) {
      throw new ApiError(httpStatus.NOT_FOUND, 'section details not found');
    }

    // get question set details
    const getQuesSet = await testService.questionSets(id, offset, maxLimit);
    // if question set details are not found then return error
    if (!getQuesSet.rowCount) {
      throw new ApiError(httpStatus.NOT_FOUND, 'question set details not found');
    }
    // get options details
    const getOptions = await testService.getOptionsByQuestion(getQuesSet.rows[0].id);
    // if options details are not found then return error
    if (!getOptions.rowCount) {
      throw new ApiError(httpStatus.NOT_FOUND, 'option details are not found');
    }

    // send success response
    res.json({
      totalRecords: totalQuesBySec.rowCount,
      question: getQuesSet.rows[0],
      options: getOptions.rows,
    });
  } catch (error) {
    next(error);
  }
};

// get details of attempted question and answer
const getAttemptsQuestionAnswer = async (req, res, next) => {
  try {
    const id = parseInt(req.body.id, 10);
    const section = parseInt(req.body.section, 10);
    const testCan = parseInt(req.body.test_can, 10);
    const pubID = parseInt(req.body.pub_id, 10);
    // check mandatory parameters
    if (
      !id ||
      Number.isNaN(id) ||
      !section ||
      Number.isNaN(section) ||
      !testCan ||
      Number.isNaN(testCan) ||
      !pubID ||
      Number.isNaN(pubID)
    ) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'mandatory parameter missing');
    }

    // get details of attempted question answer
    const isAttem = await testService.checkAttemptsQuestion(id, section, testCan, pubID);
    // return if attempted question answer not found
    if (isAttem.rowCount === 0) {
      res.json({});
      return;
    }
    // get the options details from DB
    const getOption = await testService.getAttemptsQuestionAnswer(id, section, testCan, pubID);
    // send successful response
    res.json(getOption.rows[0]);
  } catch (error) {
    next(error);
  }
};

// update the provided answer into DB
const giveTheAnswer = async (req, res, next) => {
  try {
    const testCan = parseInt(req.body.test_can, 10);
    const pubID = parseInt(req.body.pub_id, 10);
    const secID = parseInt(req.body.sec_id, 10);
    const quesID = parseInt(req.body.ques_id, 10);
    const optionID = parseInt(req.body.option_id, 10);
    const quesMarks = parseFloat(req.body.ques_marks, 10);

    // check in DB whether attempted question answer exists
    const isAttem = await testService.checkAttemptsQuestion(quesID, secID, testCan, pubID);

    // if the question answer already exists then update
    // otherwise insert
    if (isAttem.rowCount) {
      const updateAnswer = await testService.givenAnswerUpdate(
        quesID,
        secID,
        optionID,
        testCan,
        pubID
      );
      res.json(updateAnswer.rows[0]);
    } else {
      const runTest = await testService.insertIntoRunTest(pubID, secID, quesID, testCan, quesMarks);
      const runTestOption = await testService.insertIntoRunTestOption(runTest.rows[0].id, optionID);
      res.json(runTestOption.rows[0]);
    }
  } catch (error) {
    next(error);
  }
};

// update finished flag in DB
const finishedAnswer = async (req, res, next) => {
  try {
    let { id } = req.body;
    id = parseInt(id, 10);
    if (!id || Number.isNaN(id)) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'id is required');
    }
    // update end test flag in DB
    await testService.finishedTest(id);
    res.sendStatus(200);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getTotalSection,
  questionSets,
  getAttemptsQuestionAnswer,
  giveTheAnswer,
  finishedAnswer,
};
