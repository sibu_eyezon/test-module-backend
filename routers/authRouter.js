const router = require('express').Router();
const { authController } = require('../controllers');

router.post('/', authController.passcodeAuthentication);

module.exports = router;
