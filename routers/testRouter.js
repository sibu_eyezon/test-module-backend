const router = require('express').Router();
const endpoints = require('../config/endponits');
const { testController } = require('../controllers');

router.post(endpoints.SECTION, testController.getTotalSection);
router.post(endpoints.QUESTION, testController.questionSets);
router.post(endpoints.QUES_ATTEMPT, testController.getAttemptsQuestionAnswer);
router.post(endpoints.GIVE_ANS, testController.giveTheAnswer);
router.post(endpoints.FINISH, testController.finishedAnswer);

module.exports = router;
