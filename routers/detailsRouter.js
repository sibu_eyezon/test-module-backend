const router = require('express').Router();
const endpoints = require('../config/endponits');
const { detailsController } = require('../controllers');

router.post(endpoints.USER_DETAILS, detailsController.getUserDetails);
router.post(endpoints.EXAM_DETAILS, detailsController.getExamDetails);
router.post(endpoints.START_EXAM, detailsController.startExam);

module.exports = router;
