const authRouter = require('./authRouter');
const detailsRouter = require('./detailsRouter');
const testRouter = require('./testRouter');

module.exports = { authRouter, detailsRouter, testRouter };
