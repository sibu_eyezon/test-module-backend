const pg = require('pg');
const { postgres } = require('./config/config');

const { Pool, types } = pg;
types.setTypeParser(1114, (stringValue) => stringValue);

const pool = new Pool({
  user: postgres.user,
  host: postgres.host,
  database: postgres.database,
  password: postgres.password,
  port: postgres.port,
});

module.exports = pool;
