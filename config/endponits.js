const endpoints = {};
endpoints.PING = '/ping';
endpoints.AUTH_API = '/auth_api';
endpoints.DETAILS_API = '/details_api';
endpoints.EXAM_API = '/exam_api';
endpoints.USER_DETAILS = '/user_details';
endpoints.EXAM_DETAILS = '/exam_details';
endpoints.START_EXAM = '/start_exam';
endpoints.SECTION = '/section';
endpoints.QUESTION = '/question';
endpoints.QUES_ATTEMPT = '/question_attemp';
endpoints.GIVE_ANS = '/give_answer';
endpoints.FINISH = '/finished';

module.exports = endpoints;
