require('dotenv').config();

const port = process.env.PORT || 8080;
const nodeEnv = process.env.NODE_ENV;
const secretKey = process.env.JWT_SECRET;
const tokenExpiry = process.env.JWT_EXPIRY || '3h';

const postgres = {
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_DATABASE,
};

module.exports = {
  port,
  postgres,
  nodeEnv,
  secretKey,
  tokenExpiry,
};
