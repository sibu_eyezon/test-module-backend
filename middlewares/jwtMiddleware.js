const httpStatus = require('http-status');
const { verifyToken } = require('../services/tokenService');
const ApiError = require('../utils/ApiError');

// validate the Bearer token
// if valid token then set the token data into req.user
const isSignedIn = async (req, res, next) => {
  try {
    const payload = await verifyToken(req.headers.authorization);
    req.user = payload;
    next();
  } catch (err) {
    next(new ApiError(httpStatus.UNAUTHORIZED, err.message, err.stack));
  }
};

module.exports = { isSignedIn };
