const httpStatus = require('http-status');
const config = require('../config/config');
const ApiError = require('../utils/ApiError');

// convert other error types to ApiError
const errorConverter = (err, req, res, next) => {
  let error = err;

  if (!(error instanceof ApiError)) {
    const statusCode = error.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
    const message = error.message || httpStatus[statusCode];
    error = new ApiError(statusCode, message, err.stack);
  }

  next(error);
};

// error handler middleware function
const errorHandler = (err, req, res, next) => {
  let { statusCode } = err;
  const { message } = err;
  if (!statusCode) {
    statusCode = httpStatus.INTERNAL_SERVER_ERROR;
  }
  const response = { code: statusCode, message };
  res.locals.errorMessage = err.message;
  if (config.nodeEnv === 'development') {
    response.stack = err.stack;
  }
  console.error(err);
  res.status(statusCode).send(response);
  next();
};

module.exports = {
  errorConverter,
  errorHandler,
};
