const express = require('express');
const endpoints = require('./config/endponits');
const { isSignedIn } = require('./middlewares/jwtMiddleware');
const { authRouter, detailsRouter, testRouter } = require('./routers');

const routers = express.Router();

// ping route for debugging purpose
routers.use(endpoints.PING, (_req, res) => {
  res.json({ success: true, message: 'Node server is running!!' });
});
routers.use(endpoints.AUTH_API, authRouter);
routers.use(endpoints.DETAILS_API, isSignedIn, detailsRouter);
routers.use(endpoints.EXAM_API, isSignedIn, testRouter);

module.exports = routers;
